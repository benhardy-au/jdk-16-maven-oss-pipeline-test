package net.bhardy.java.jdk16;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world!
 */
public final class App {
    /**
     * Yay, it's a logger. TODO make checkstyle not care about this.
     */
    private static final Logger LOG = Logger.getLogger("main");

    /**
     * Main entrypoint.
     *
     * @param args - command line args.
     */
    public static void main(final String[] args) {
        LOG.log(Level.INFO, "Hello World!");
    }

    private App() {
    }
}

