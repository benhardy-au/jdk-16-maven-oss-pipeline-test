# README #

This is just a test of building a Maven project with Java 16 and publishing it to Maven Central.

### What is this repository for? ###

To make the code publicly available as a demo.

### How do I get set up? ###

* Install maven 
* Clone this
* run "mvn install"

### Contribution guidelines ###

Send me a PR.

### Who do I talk to? ###

* ben at hardy dot sydney

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=benhardy-au_jdk-16-maven-oss-pipeline-test)